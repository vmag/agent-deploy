## Portainer

Add name and choose Docker image:

![Name](images/name.png)


Container must user HOST network:

![NET](images/network.png)


Add additional capabilities:

![CAP](images/cap.png)


Add ENV variables:

![ENV](images/env.png)

Click a "Deploy the container" button:

![DEPLOY](images/deploy.png)

