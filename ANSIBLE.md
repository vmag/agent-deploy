# Ansible Role: noia-agent

An Ansible Role that installs Memcached on RedHat/CentOS or Debian/Ubuntu Linux.

## Install

    ansible-galaxy install noia.agent

## Role Variables

Available variables are listed below (with dummy data).
API key:

    api_key: z99CuiZnMhe2qtz4LLX43Gbho5Zu9G8oAoWRY68WdMTVB9GzuMY2HNn667A752EA

Role:

    role: gateway

List of networks to join. _f network_id = 0 , then node will not join any network:

    network_id:
      - Lpy3zq2ehdVZehZvoRFur4tV
      - U7FrPST7bV6NQGyBdhHyiebg
    network_type: one2many

Metadata (optional)

    name: Azure EU gateway
    country: Germany
    city: Frankfurt
    provider: Azure
    lat: 40.14
    lon:  -74.21


## Dependencies

None.

## Example Playbook

    - hosts: noia-agent
      roles:
        - { role: noia.agent }