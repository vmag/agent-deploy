**Run NOIA Agent in docker container**

    docker run --network="host"  --restart=on-failure:10 --cap-add=NET_ADMIN --cap-add=NET_ADMIN --cap-add=SYS_MODULE \ 
    --name=noia-agent \
    -e 'API_KEY=z99CuiZnMhe2qtz4LLX43Gbho5Zu9G8oAoWRY68WdMTVB9GzuMY2HNn667A752EA' \ 
    -e 'ROLE=gateway' \
    -e 'NETWORK_ID = ["Lpy3zq2ehdVZehZvoRFur4tV","U7FrPST7bV6NQGyBdhHyiebg"]' \
    -e 'NETWORK_TYPE=one2many' \
    -e 'CITY=Frankfurt' \
    -d noia/agent

---