**Install NOIA Agent**

`pip install noia-agent`


---

**Edit a `/etc/noia-agent/config.ini` file**


    [API]
    api_key = z99CuiZnMhe2qtz4LLX43Gbho5Zu9G8oAoWRY68WdMTVB9GzuMY2HNn667A752EA

    #Possible roles are: device, gateway
    [ROLES]
    role = gateway

    #If network_id = 0 , then node will not join any network. Network ID's must be generated in NOIA platform. Device can join multiple networks
    #Network type: one2many, mesh, p2p
    [NETWORK]
    network_id = ['Lpy3zq2ehdVZehZvoRFur4tV','U7FrPST7bV6NQGyBdhHyiebg']
    network_type = one2many
    
    #Optional meta data:
    [META]
    name = Azure EU gateway
    country = Germany
    city = Frankfurt
    provider = Azure
    lat = 40.14
    lon =  -74.21
	
---

**Start a NOIA Agent**


 `systemctl start noia-agent`

---


